$(function() {
	var dec_height = function(first) {
        var content = $('body').height(),
            win_h = $(window).height(),
            main_h = $('#main').outerHeight(),
            need_h = win_h - content;
        if (content < win_h || typeof first != 'undefined') {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    dec_height(true);
    $('#pro-series .list-group-item').on('click', function() {
        dec_height();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('#pro-series .list-group-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.ellipsis2').ellipsis({
        row: 2
    });
});