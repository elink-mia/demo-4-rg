(function($) {
    var dec_height = function() {
        var content = $('body').height(),
            win_h = $(window).height(),
            main_h = $('#main').outerHeight(),
            need_h = win_h - content;
        if (content < win_h) {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    var no_scroll = function() {
        var win_h = $(window).height(),
            //nav_h = $('#navbar.show').outerHeight(),
            nav_h = $('#navbar.show').outerHeight();
        if (nav_h > win_h) {
            $('body').removeClass("no-scroll");
        } else {
            $('body').addClass("no-scroll");
        }
    };
    $(window).on('load', function() {
        var winW = $(window).width();
        dec_height();
    });
    $(window).resize(function() {
        var winW = $(window).width();
        dec_height();
    });
    var burgerMenu = function() {
        $('.navbar-toggler').click(function() {
            if ($('#navbar').hasClass('show')) {
                $('body').removeClass("no-scroll");
                $('.fa.fa-times').addClass('d-none');
                $('.fa.fa-bars').removeClass('d-none');
                $('.dropdown-menu').removeClass('active');
            } else {
                $('body').addClass("no-scroll");
                $('.fa.fa-bars').addClass('d-none');
                $('.fa.fa-times').removeClass('d-none');
            }
        });
    };
    var hover = function() {
        var $win = $(window).width();
        if ($win > 992) {
            $(".dropdown").hover(function() {
                    //Add a class of current and fade in the sub-menu
                $(this).find(".dropdown-menu").css({
                    'width': '100%',
                    'display': 'block',
                    'margin': '0',
                    'opacity': '1',
                    'z-index': '1',
                    'transform': 'translateY(%)'
                });
                $('.dropdown .dropdown-toggle').removeClass('active');
                $(this).find(".dropdown-toggle").addClass('active');
            }, function() {
                $(this).find(".dropdown-menu").css('display', 'none');
                $(this).find(".dropdown-toggle").removeClass('active');
            });
        }
        if ($win < 992) {
            $(".dropdown").click(function() {
                if ($(this).find('.dropdown-menu').hasClass('active')) {
                    $(this).find('.dropdown-menu').removeClass('active');
                } else {
                    $(".dropdown-menu.active").removeClass('active');
                    $(this).find('.dropdown-menu').addClass('active');    
                }
            });
        }
    };
    var winScroll = function() {
        var test = $('body').height(),
            winH = $(window).height(),
            headerH = $('#header').height(),
            x = test - winH;
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 1 && x >= headerH) {
                $('#header').addClass('fixed');
            }
            if ($(window).scrollTop() <= 0) {
                $('#header').removeClass('fixed');
            }
        });
    };
    new WOW().init();
    var goToTop = function() {
        $(window).scroll(function() {
            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('#in-btn .js-gotop').addClass('active');
            } else {
                $('#in-btn .js-gotop').removeClass('active');
            }
        })
        $('.js-gotop').on('click', function(event) {

            event.preventDefault();

            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');

            return false;
        });;
    };
    $(function() {
        burgerMenu();
        hover();
        winScroll();
        goToTop();
    });
})(jQuery)